import App
import Btrfs
import BtrfsCli
import Config
import Data.Functor (void)
import Data.Map as Map
import Data.Set as Set
import Data.Time
import Logger
import Parser.Internal as Parser
import Polysemy as P
import Polysemy.Trace
import Scheduler
import Test.Hspec
import Time

main :: IO ()
main = hspec suite

suite :: Spec
suite = do
  describe "Parser" do
    let expectSucceedWith value (P parse) input remaining = parse input `shouldBe` Good remaining value
    let expectSucceed parser = expectSucceedWith () (void parser)
    let expectFailed (P parse) input = void (parse input) `shouldBe` Bad

    it "succeed" $
      expectSucceed (succeed ()) "" ""

    it "failed" $
      expectFailed  failed ""

    it "oneOf" do
      expectSucceed (oneOf [keyword "yes", keyword "yeah"]) "yes"  ""
      expectSucceed (oneOf [keyword "yes", keyword "yeah"]) "yeah" ""
      expectFailed  (oneOf [keyword "yes", keyword "yeah"]) "nope"

    it "many" do
      expectSucceed (many $ keyword "yes") "yes"        ""
      expectSucceed (many $ keyword "yes") "yesyes"     ""
      expectSucceed (many $ keyword "yes") "yesyesnope" "nope"
      expectFailed  (many $ keyword "yes") "nope"

    it "manyOrZero" do
      expectSucceed (manyOrZero $ keyword "yes") "yes"        ""
      expectSucceed (manyOrZero $ keyword "yes") "yesyes"     ""
      expectSucceed (manyOrZero $ keyword "yes") "yesyesnope" "nope"
      expectSucceed (manyOrZero $ keyword "yes") "nope"       "nope"

    it "repeat" do
      expectFailed  (Parser.repeat 0 $ keyword "yes") "yes"
      expectSucceed (Parser.repeat 1 $ keyword "yes") "yes"        ""
      expectFailed  (Parser.repeat 2 $ keyword "yes") "yes"
      expectSucceed (Parser.repeat 2 $ keyword "yes") "yesyes"     ""
      expectSucceed (Parser.repeat 2 $ keyword "yes") "yesyesnope" "nope"
      expectFailed  (Parser.repeat 2 $ keyword "yes") "nope"

    it "recover" do
      expectSucceed (Parser.recover "yes" $ keyword "yes") "yes"  ""
      expectSucceed (Parser.recover "yes" $ keyword "yes") "nope" "nope"

    it "keyword" do
      expectSucceed (keyword "yes") "yes"  ""
      expectFailed  (keyword "yes") "nope"

    it "symbol" do
      expectSucceed (symbol "yes") "yes" ""
      expectFailed  (symbol "yes") "nope"

    it "whitespaces" do
      expectSucceed whitespaces " \t \t" ""
      expectFailed  whitespaces "nope"

    it "lineReturn" do
      expectSucceed lineReturn "\n"   ""
      expectSucceed lineReturn "\r\n" ""
      expectFailed  lineReturn "nope"

    it "charIf" do
      expectSucceed (charIf (== 'a')) "a" ""
      expectFailed  (charIf (== 'a')) "b"
    it "while" do
      expectSucceed (while (== 'a')) "aab" "b"
      expectFailed  (while (== 'a')) "b"

    it "optional" do
      expectSucceedWith (Just "yes") ((optional $ keyword "yes") |. keyword "nope") "yesnope" ""
      expectSucceedWith Nothing      ((optional $ keyword "yes") |. keyword "nope") "nope"    ""
      expectSucceedWith (Just "yes") ((optional $ succeed "yes") |. keyword "nope") "nope"    ""
      expectSucceedWith Nothing      (void <$> optional failed   |. keyword "nope") "nope"    ""

      expectFailed                   ((optional $ keyword "yes") |. keyword "nope") "yes"
      expectFailed                   ((optional $ keyword "yes") |. keyword "nope") ""
      expectFailed                   ((optional $ succeed "yes") |. keyword "nope") ""
      expectFailed                   (void <$> optional failed   |. keyword "nope") ""

    it "ignore" do
      expectSucceed (succeed () |. keyword "yes" |. keyword "yes") "yesyes"  ""
      expectFailed  (succeed () |. keyword "yes" |. keyword "yes") "yesnope"
      expectFailed  (succeed () |. keyword "yes" |. keyword "yes") "nope"

    it "keep" do
      expectSucceedWith "yesyes" (succeed (<>) |= keyword "yes" |= keyword "yes") "yesyes" ""
      expectFailed               (succeed (<>) |= keyword "yes" |= keyword "yes") "yesnope"
      expectFailed               (succeed (<>) |= keyword "yes" |= keyword "yes") "nope"

  describe "Config" do
    configFile <- runIO $ readFile "test/resources/btrfs-backup.conf"

    let snapshotSchedule = Schedule All (Nb 12) (Nb 4) (Nb 7) None
    let config = Map.fromList
          [ ( "/mnt/ssd", Map.fromList
              [ ( "root", SubvolumeConfig "snapshots" snapshotSchedule []
                )
              , ( "home", SubvolumeConfig "snapshots" snapshotSchedule []
                )
              ]
            )
          , ( "/mnt/hdd", Map.fromList
              [ ( "media", SubvolumeConfig "snapshots" snapshotSchedule []
                )
              ]
            )
          ]

    it "Can load config" do
      let (logged, actualConfig) = run . runLoggerPure $ toConfig configFile
      actualConfig `shouldBe` config
      logged `shouldBe` [ "Warning: Unknown config key \"unhandled_key\", ignoring."
                        , "Warning: Failed to parse value for key \"snapshot_preserve\", ignoring."
                        ]

  describe "BtrfsCli" do
    let expected =
         [ Subvolume "snapshots/root.0001-01-01T00:00" "77d15cf3-58c5-0e4d-b225-87d700c028a7" "6540588f-6143-2243-ac51-a37a523deece" Nothing
         , Subvolume "snapshots/home.0001-01-01T00:00" "6f66aa44-f098-3c4f-a2fb-72c87e038d3f" "f28897dd-1baa-084d-a3d1-f2e47dc606c3" Nothing
         , Subvolume "snapshots/root.0001-01-02T00:00" "82b7d365-bd10-624e-bdfe-1b0b37986b88" "6540588f-6143-2243-ac51-a37a523deece" Nothing
         , Subvolume "snapshots/home.0001-01-02T00:00" "99f36407-317f-174f-8334-39a490e20d8a" "f28897dd-1baa-084d-a3d1-f2e47dc606c3" Nothing
         ]

    input <- runIO $ readFile "test/resources/btrfs-subvolume-list.txt"

    it "Can parse result of btrfs subvolume list command" do
      runParser parserSubvolumeList input `shouldBe` Just expected

  describe "Schedule" do
    let snapshots =
          [ "snapshots/root.0001-01-01T01:01"
          , "snapshots/root.0002-01-01T01:01"
          , "snapshots/root.0002-02-01T01:01"
          ]

    it "Ignores snapshots name that fail to parse" $
      schedule "root" (Schedule None None None None None) [ "invalid snapshot name" ]
        `shouldBe` ScheduleResult { kept = [], removed = [] }

    it "Keeps nothing when schedule is none" $
      schedule "root"
        (Schedule None None None None None)
        snapshots
      `shouldBe`
        ScheduleResult
          { kept    = []
          , removed =
              [ "root.0001-01-01T01:01"
              , "root.0002-01-01T01:01"
              , "root.0002-02-01T01:01"
              ]
          }

    it "Keeps all when schedule is all" $
      schedule "root"
        (Schedule All None None None None)
        snapshots
      `shouldBe`
        ScheduleResult
          { kept    =
              [ "root.0001-01-01T01:01"
              , "root.0002-01-01T01:01"
              ]
          , removed =
              [ "root.0002-02-01T01:01"
              ]
          }

    it "Keeps only one when schedule is one" $
      schedule "root"
        (Schedule (Nb 1) None None None None)
        snapshots
      `shouldBe`
        ScheduleResult
          { kept    =
              [ "root.0002-01-01T01:01" ]
          , removed =
              [ "root.0001-01-01T01:01"
              , "root.0002-02-01T01:01"
              ]
          }

  describe "Application" do
    let snapshotSchedule = Schedule None (Nb 2) None None None
    let targetSchedule   = Schedule None None None None None
    let config = Map.fromList
          [ ( "/mnt/ssd", Map.fromList
              [ ( "root", SubvolumeConfig "snapshots" snapshotSchedule
                  [ TargetConfig "" targetSchedule ]
                )
              , ( "home", SubvolumeConfig "snapshots" snapshotSchedule
                  [ TargetConfig "" targetSchedule ]
                )
              ]
            )
          , ( "/mnt/hdd", Map.fromList
              [ ( "media", SubvolumeConfig "snapshots" snapshotSchedule
                  [ TargetConfig "" targetSchedule ]
                )
              ]
            )
          ]

    let expected :: FilePath -> [Subvolume]
        expected = \case
          "/mnt/ssd" ->
            [ Subvolume "snapshots/root.0001-01-02T00:00" "" "" Nothing
            , Subvolume "snapshots/root.0001-01-01T00:00" "" "" Nothing
            , Subvolume "snapshots/home.0001-01-02T00:00" "" "" Nothing
            , Subvolume "snapshots/home.0001-01-01T00:00" "" "" Nothing
            ]
          "/mnt/hdd" ->
            [ Subvolume "snapshots/media.0001-01-02T00:00" "" "" Nothing
            , Subvolume "snapshots/media.0001-01-01T00:00" "" "" Nothing
            ]
          _ -> []

    let subvolumeList = Map.fromList [( "/mnt/ssd", expected "/mnt/ssd"), ("/mnt/hdd", expected "/mnt/hdd")]

    let now = UTCTime (fromGregorian 1 2 1) 0

    let schedules = buildSnapshotSchedule now config subvolumeList

    let runBtrfsLogged :: Sem (Btrfs ': effs) a -> Sem effs [String]
        runBtrfsLogged = fmap fst . runTraceList . reinterpret \case
          ReadVolumeInfo volume            -> do
            log ["ReadVolumeInfo", volume]
            pure (expected volume)
          CreateSnapshot (source, target)  -> log ["CreateSnapshot", source, target]
          DeleteSnapshot target            -> log ["DeleteSnapshot", target]
          RestoreSnapshot (source, target) -> log ["RestoreSnapshot", source, target]
          SendBackup (source, target)      -> log ["SendBackup", source, target]
          DeleteBackup target              -> log ["DeleteBackup", target]
          where
            log :: Member Trace effs => [String] -> Sem effs ()
            log x = trace $ unwords x

    let runBackup :: [String]
        runBackup = P.run
          . runConfStub config
          . runTimeStub now
          . runBtrfsLogged
          $ backup

    it "volumesToRead" $
      volumesToRead config `shouldBe`
        Set.fromList ["/mnt/ssd", "/mnt/hdd"]

    it "groupBySubvolume" $
      groupBySubvolume <$> subvolumeList `shouldBe`
        Map.fromList
          [ ( "/mnt/hdd", Map.fromList
                [ ("media", ["snapshots/media.0001-01-01T00:00", "snapshots/media.0001-01-02T00:00"])
                ]
            )
          , ( "/mnt/ssd", Map.fromList
                [ ("home", ["snapshots/home.0001-01-01T00:00", "snapshots/home.0001-01-02T00:00"])
                , ("root", ["snapshots/root.0001-01-01T00:00", "snapshots/root.0001-01-02T00:00"])
                ]
            )
          ]

    it "buildSchedule" $
      schedules `shouldBe` Map.fromList
        [ ("/mnt/hdd", Map.fromList
            [ ("media", ScheduleResult ["media.0001-01-01T00:00", "media.0001-02-01T00:00"] ["media.0001-01-02T00:00"])
            ]
          )
        , ("/mnt/ssd", Map.fromList
            [ ("home", ScheduleResult ["home.0001-01-01T00:00", "home.0001-02-01T00:00"] ["home.0001-01-02T00:00"])
            , ("root", ScheduleResult ["root.0001-01-01T00:00", "root.0001-02-01T00:00"] ["root.0001-01-02T00:00"])
            ]
          )
        ]

    it "snapshotsToCreate" $
      snapshotsToCreate now config schedules `shouldBe`
        [ ( "/mnt/hdd/media", "/mnt/hdd/snapshots/media.0001-02-01T00:00" )
        , ( "/mnt/ssd/home", "/mnt/ssd/snapshots/home.0001-02-01T00:00" )
        , ( "/mnt/ssd/root", "/mnt/ssd/snapshots/root.0001-02-01T00:00" )
        ]

    it "snapshotsToDelete" $
      snapshotsToDelete config schedules `shouldBe`
        [ "/mnt/hdd/snapshots/media.0001-01-02T00:00"
        , "/mnt/ssd/snapshots/home.0001-01-02T00:00"
        , "/mnt/ssd/snapshots/root.0001-01-02T00:00"
        ]

    it "backup" $ -- Todo add next commands when supported
      runBackup `shouldBe`
        [ "ReadVolumeInfo /mnt/hdd"
        , "ReadVolumeInfo /mnt/ssd"
        , "CreateSnapshot /mnt/hdd/media /mnt/hdd/snapshots/media.0001-02-01T00:00"
        , "CreateSnapshot /mnt/ssd/home /mnt/ssd/snapshots/home.0001-02-01T00:00"
        , "CreateSnapshot /mnt/ssd/root /mnt/ssd/snapshots/root.0001-02-01T00:00"
        , "DeleteSnapshot /mnt/hdd/snapshots/media.0001-01-02T00:00"
        , "DeleteSnapshot /mnt/ssd/snapshots/home.0001-01-02T00:00"
        , "DeleteSnapshot /mnt/ssd/snapshots/root.0001-01-02T00:00"
        ]
