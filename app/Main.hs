import App
import Btrfs
import Command
import Config
import Error
import FileSystem
import Logger
import Options.Applicative
import Polysemy
import Time

main :: IO ()
main = do
  args <- execParser cli

  case cmd args of
    Run _   -> runner args backup
    Restore -> runner args restore

  where
    runner args =
      runM
        . runError
        . runLogger
        . runFileSystem
        . runCommand (dryRun args)
        . runTime
        . runConf (configurationPath args)
        . runBtrfs

data Cli = Cli
  { configurationPath :: FilePath
  , dryRun            :: Bool
  , cmd               :: CliCommand
  }

data CliCommand
  = Run RunArg
  | Restore

newtype RunArg = RunArg
  { progress :: Bool
  }

cli :: ParserInfo Cli
cli = info (helper <*> version <*> cli) (fullDesc <> description)
  where
    description = header "Snapshots and backups to local or distant disks for your btrfs and zfs filesystems"
    version = infoOption "v0.1.0"
      (  short 'v'
      <> long  "version"
      <> help  "Show program version"
      )

    cli = Cli
      <$> strOption
        (  short   'c'
        <> long    "configuration"
        <> metavar "FILE"
        <> help    "Path to the configuration file"
        <> value   "/etc/btrfs-backup.conf"
        <> showDefault
        )
      <*> switch
        (  short   'd'
        <> long    "dry-run"
        <> help    "Do not perform any actions, only show what would be done"
        )
      <*> hsubparser
        (  command "run"     (info run     (progDesc "Perform snapshot and backup operations"))
        <> command "restore" (info restore (progDesc "Restore a snapshot or a backup"))
        )

    run = Run . RunArg
      <$> switch
        (  short 'p'
        <> long  "progress"
        <> help "Show progress information (global progress and current operation progress)"
        )

    restore = pure Restore
