module Scheduler
  ( SnapshotMoment (..)
  , Schedule (..)
  , ScheduleNb (..)
  , ScheduleResult (..)
  , MomentGranularity (..)
  , schedule
  , toSubvolume
  ) where

import           Data.Function ((&), on)
import           Data.List (groupBy, sort, sortOn)
import           Data.Maybe
import           Data.Ord
import qualified Data.Set as Set
import           Data.Time (UTCTime(UTCTime), fromGregorian, secondsToDiffTime)
import           Data.Time.Calendar.WeekDate (toWeekDate)
import           Data.Time.Clock
import           Parser
import           Time

data SnapshotMoment = SnapshotMoment
  { year   :: Int
  , month  :: Int
  , week   :: Int
  , day    :: Int
  , hour   :: Int
  , minute :: Int
  }
  deriving (Eq, Ord)

instance Show SnapshotMoment where
  show SnapshotMoment{year, month, day, hour, minute} =
    formatTime $ toUtcTime year month day hour minute

toUtcTime :: Int -> Int -> Int -> Int -> Int -> UTCTime
toUtcTime year month day hour minute =
  UTCTime
    (fromGregorian (toInteger year) month day)
    (secondsToDiffTime $ toInteger $ hour * 3600 + minute * 60)

data Schedule = Schedule
  { perYear  :: ScheduleNb
  , perMonth :: ScheduleNb
  , perWeek  :: ScheduleNb
  , perDay   :: ScheduleNb
  , perHour  :: ScheduleNb
  }
  deriving (Eq, Show)

data ScheduleNb
  = Nb Int
  | All
  | None
  deriving (Eq, Show)

data ScheduleResult = ScheduleResult
  { kept    :: [FilePath]
  , removed :: [FilePath]
  }
  deriving (Eq, Show)

data MomentGranularity
  = Year
  | Month
  | Week
  | Day
  | Hour

schedule :: FilePath -> Schedule -> [FilePath] -> ScheduleResult
schedule subvolume schedule snapshots =
  ScheduleResult
    { kept    = Set.toList kept
    , removed = Set.toList removed
    }
  where
    snapshotMoments = mapMaybe toMoment snapshots
    allMoments      = Set.fromList $ toFilePath <$> snapshotMoments
    kept            = Set.fromList $ toFilePath <$> filterToKeep schedule snapshotMoments
    removed         = allMoments `Set.difference` kept
    toFilePath m    = subvolume <> "." <> show m

toSubvolume :: FilePath -> Maybe FilePath
toSubvolume = runParser (fst <$> snapshotNameParser)

toMoment :: FilePath -> Maybe SnapshotMoment
toMoment = runParser (snd <$> snapshotNameParser)

snapshotNameParser :: Parser (FilePath, SnapshotMoment)
snapshotNameParser =
  succeed buildSnapshotMoment
    |. leadingPath
    |= word
    |. symbol "."
    |= int
    |. symbol "-"
    |= int
    |. symbol "-"
    |= int
    |. symbol "T"
    |= int
    |. symbol ":"
    |= int
  where
    buildSnapshotMoment path year month day hour minute =
      ( path
      , SnapshotMoment year month (fromInteger week) day hour minute
      )
      where (week, _, _) = toWeekDate $ utctDay $ toUtcTime year month day hour minute

leadingPath :: Parser [FilePath]
leadingPath =
  manyOrZero $ while (/= '/') |. charIf (const True)

word :: Parser String
word = while (`elem` ['a' .. 'z'] <> ['A' .. 'Z'])

filterToKeep :: Schedule -> [SnapshotMoment] -> [SnapshotMoment]
filterToKeep schedule allMoments =
  deduplicate . concat $
    [ filterToKeepForGranularity Year  schedule allMoments
    , filterToKeepForGranularity Month schedule allMoments
    , filterToKeepForGranularity Week  schedule allMoments
    , filterToKeepForGranularity Day   schedule allMoments
    , filterToKeepForGranularity Hour  schedule allMoments
    ]

deduplicate :: Ord a => [a] -> [a]
deduplicate = Set.toList . Set.fromList

filterToKeepForGranularity :: MomentGranularity -> Schedule -> [SnapshotMoment] -> [SnapshotMoment]
filterToKeepForGranularity granularity schedule allMoments =
  allMoments
    & groupByGranularity granularity
    & mapMaybe keepFirst
    & sortDesc
    & takeScheduleNb (nbToKeep granularity schedule)

groupByGranularity :: MomentGranularity -> [SnapshotMoment] -> [[SnapshotMoment]]
groupByGranularity granularity =
  groupBy ((==) `on` index)
  where
    index a = case granularity of
      Year  -> (year a, 0,       0,      0,     0     )
      Month -> (year a, month a, 0,      0,     0     )
      Week  -> (year a, month a, week a, 0,     0     )
      Day   -> (year a, month a, week a, day a, 0     )
      Hour  -> (year a, month a, week a, day a, hour a)

keepFirst :: Ord a => [a] -> Maybe a
keepFirst = maybeHead . sort

maybeHead :: [a] -> Maybe a
maybeHead = \case
  head:_ -> Just head
  []     -> Nothing

sortDesc :: Ord a => [a] -> [a]
sortDesc = sortOn Down

nbToKeep :: MomentGranularity -> Schedule -> ScheduleNb
nbToKeep = \case
  Year  -> perYear
  Month -> perMonth
  Week  -> perWeek
  Day   -> perDay
  Hour  -> perHour

takeScheduleNb :: ScheduleNb -> [a] -> [a]
takeScheduleNb = \case
  Nb nb -> take nb
  All   -> id
  None  -> const []
