module Utils
  ( path
  , Map2
  , map2Map
  , map2Flatten
  , map2FlattenMap
  , map2IntersectionWithKey
  , map2LeftIntersection
  , groupByKey
  , (.:)
  , capitalize, trim
  ) where

import           Control.Monad (join)
import           Data.Char
import           Data.List
import           Data.Map.Strict (Map)
import           Data.Map.Merge.Strict as Map
import qualified Data.Map.Strict as Map

-- | Builds a file path from parts
path :: [FilePath] -> FilePath
path = intercalate "/"

-- Map utils

type Map2 k v = Map k (Map k v)

map2Map :: (k -> k -> v -> w) -> Map2 k v -> Map2 k w
map2Map f =
  Map.mapWithKey
    \k1 -> Map.mapWithKey (f k1)

map2Flatten :: Map2 k v -> [v]
map2Flatten =
  join . Map.elems . Map.map Map.elems

map2FlattenMap :: (k -> k -> v -> w) -> Map2 k v -> [w]
map2FlattenMap f =
  join . Map.elems . Map.mapWithKey
    \k1 -> Map.elems . Map.mapWithKey (f k1)

map2IntersectionWithKey :: Ord k
  => (k -> k -> vl -> vr -> w)
  -> Map2 k vl
  -> Map2 k vr
  -> Map2 k w
map2IntersectionWithKey f =
  Map.intersectionWithKey $
    Map.intersectionWithKey . f

map2LeftIntersection :: (Ord k, Monoid v) => Map2 k v -> Map2 k v -> Map2 k v
map2LeftIntersection =
  Map.merge preserveMissing dropMissing (zipWithMatched $
    const $ Map.merge preserveMissing dropMissing (zipWithMatched $ const (<>))
  )

groupByKey :: Ord k => [(k, v)] -> Map k [v]
groupByKey = foldl (\m (k, v) -> Map.insertWith (++) k [v] m) Map.empty

-- Composition

infixr 8 .:
(.:) :: (c -> d) -> (a -> b -> c) -> a -> b -> d
(.:) g f x y = g (f x y)

-- String

capitalize :: String -> String
capitalize = \case
  []       -> []
  h : tail -> toUpper h : tail

trim :: String -> String
trim = dropWhileEnd isSpace . dropWhile isSpace
