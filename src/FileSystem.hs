module FileSystem
  ( FileSystem
  , FileSystem.readFile
  , runFileSystem
  ) where

import Control.Exception
import Polysemy
import System.IO as IO

data FileSystem m a where
  ReadFile :: FilePath -> FileSystem m (Either IOError String)
makeSem ''FileSystem

runFileSystem :: Member (Embed IO) effs => Sem (FileSystem ': effs) a -> Sem effs a
runFileSystem = interpret \case
  ReadFile path -> embed . try $ IO.readFile path
