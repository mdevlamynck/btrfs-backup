module App where

import           Btrfs
import           BtrfsCli
import           Config
import           Control.Monad (join)
import           Data.Function ((&))
import           Data.Functor ((<&>))
import           Data.List (isInfixOf)
import           Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import           Data.Maybe
import           Data.Semigroup ((<>))
import           Data.Set (Set)
import           Polysemy
import           Scheduler
import           Time
import           Utils
import           Prelude hiding (log)

-- Backup

backup :: Members '[Conf, Time, Btrfs] effs => Sem effs ()
backup = do
  config    <- readConf
  now       <- readCurrentTime

  snapshots <- sequence $ readVolumeInfo `Map.fromSet` volumesToRead config
  let schedules = buildSnapshotSchedule now config snapshots

  createSnapshot `mapM_` snapshotsToCreate now config schedules
  deleteSnapshot `mapM_` snapshotsToDelete config schedules

--  snapshots <- sequence $ readVolumeInfo `Map.fromSet` volumesToRead config
--  let schedules = buildSchedule now config (groupBySubvolume <$> snapshots)

--  sendBackup   `mapM_` snapshotsToCreate now config schedules
--  deleteBackup `mapM_` snapshotsToDelete config schedules

volumesToRead :: Config -> Set FilePath
volumesToRead = Map.keysSet

type Snapshots = Map2 FilePath [FilePath]
type SnapshotSchedule = Map2 FilePath ScheduleResult

buildSnapshotSchedule :: UTCTime -> Config -> Map FilePath [Subvolume] -> SnapshotSchedule
buildSnapshotSchedule now config snapshots =
    buildSchedule config $ addSnapshotsToCreate now config $ groupBySubvolume <$> snapshots

buildSchedule :: Config -> Snapshots -> SnapshotSchedule
buildSchedule =
  map2IntersectionWithKey
    \_ subvolPath SubvolumeConfig{snapshotSchedule} snapshots ->
      schedule subvolPath snapshotSchedule snapshots

groupBySubvolume :: [Subvolume] -> Map FilePath [FilePath]
groupBySubvolume =
  groupByKey . mapMaybe
    \Subvolume{subvolPath} ->
      (\subvol-> (subvol, subvolPath)) <$> toSubvolume subvolPath

addSnapshotsToCreate :: UTCTime -> Config -> Snapshots -> Snapshots
addSnapshotsToCreate now config snapshots =
  map2LeftIntersection (map2Map toCreate config) snapshots
  where
    toCreate _ subvolPath _ = [subvolPath <> "." <> formatTime now]

snapshotsToCreate :: UTCTime -> Config -> SnapshotSchedule -> [(FilePath, FilePath)]
snapshotsToCreate now =
  join . map2Flatten .: map2IntersectionWithKey
    \volumePath subvolPath SubvolumeConfig{snapshotDir} ScheduleResult{kept} ->
      kept
        & filter (isInfixOf $ formatTime now)
        <&> \snapshot ->
        ( path [ volumePath, subvolPath ]
        , path [ volumePath, snapshotDir, snapshot ]
        )

snapshotsToDelete :: Config -> SnapshotSchedule -> [FilePath]
snapshotsToDelete =
  join . map2Flatten .: map2IntersectionWithKey
    \volumePath _ SubvolumeConfig{snapshotDir} ScheduleResult{removed} ->
      removed <&> \snapshot -> path [ volumePath, snapshotDir, snapshot ]

-- Restore

restore :: Members '[Conf, Time, Btrfs] effs => Sem effs ()
restore = do
  config <- readConf
  pure ()
  -- TODO restoreSnapshot
