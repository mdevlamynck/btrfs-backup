module Parser.Internal where

import Control.Monad ((>=>))
import Data.Functor (void)
import Data.List (stripPrefix)
import Text.Read

newtype Parser value = P (String -> ParseState value)
  deriving (Functor)

instance Applicative Parser where
  pure  = succeed
  (<*>) = keep

instance Monad Parser where
  return = succeed
  (>>=)  = andThen

data ParseState value = Good String value | Bad
  deriving (Functor, Eq, Show)

-- Run Parser

runParser :: Parser value -> String -> Maybe value
runParser (P parse) input =
  case parse input of
    Good _ value -> Just value
    Bad          -> Nothing

-- Basic Constructors

succeed :: value -> Parser value
succeed = P . flip Good

failed :: Parser value
failed = P $ const Bad

-- Combinators

infixl 1 |.
(|.) :: Parser a -> Parser b -> Parser a
(|.) = ignore

infixl 1 |>
(|>) :: Parser a -> Parser b -> Parser b
(|>) = (*>)

infixl 1 |=
(|=) :: Parser (a -> b) -> Parser a -> Parser b
(|=) = keep

ignore :: Parser a -> Parser b -> Parser a
ignore = map2 const

keep :: Parser (a -> b) -> Parser a -> Parser b
keep = map2 ($)

oneOf :: [Parser value] -> Parser value
oneOf parsers = P \input -> runOneOf parsers input
  where
    runOneOf parsers input =
      case parsers of
        []                     -> Bad
        P parse : otherParsers -> case parse input of
          Good input value -> Good input value
          Bad              -> runOneOf otherParsers input

many :: Parser value -> Parser [value]
many = manyOrZero >=> \case
  []      -> failed
  results -> succeed results

manyOrZero :: Parser value -> Parser [value]
manyOrZero (P parse) = P \input -> runManyOrZero parse input
  where
    runManyOrZero parse input =
      case parse input of
        Bad              -> Good input []
        Good input value -> case runManyOrZero parse input of
          Bad               -> Good input [value]
          Good input values -> Good input (value : values)

sequence :: Parser sep -> Parser value -> Parser [value]
sequence sep parser =
  succeed (:)
    |= parser
    |= many (sep |> parser)

repeat :: Int -> Parser a -> Parser [a]
repeat n (P parse) = P \input -> runRepeat n parse input
  where
    runRepeat n parse input =
      if n <= 0 then Bad
      else           case parse input of
        Bad              -> Bad
        Good input value -> case runRepeat (n - 1) parse input of
          Bad               -> if n == 1 then Good input [value]
                               else           Bad
          Good input values -> Good input (value : values)

recover :: a -> Parser a -> Parser a
recover defaultValue parser = oneOf [ parser, succeed defaultValue ]

-- Parsers

keyword :: String -> Parser String
keyword s = P \input ->
  case stripPrefix s input of
    Nothing        -> Bad
    Just restInput -> Good restInput s

symbol :: String -> Parser ()
symbol = void . keyword

int :: Parser Int
int = while (`elem` ['0' .. '9']) >>= \s -> case readMaybe s of
  Just int -> succeed int
  Nothing -> failed

whitespaces :: Parser ()
whitespaces = void $ many $ oneOf [symbol " ", symbol "\t"]

lineReturn :: Parser ()
lineReturn = oneOf [symbol "\n", symbol "\r\n"]

charIf :: (Char -> Bool) -> Parser Char
charIf predicate = P \case
  ""             -> Bad
  c : restString -> if predicate c then Good restString c
                    else                Bad

while :: (Char -> Bool) -> Parser String
while = many . charIf

optional :: Parser a -> Parser (Maybe a)
optional = recover Nothing . fmap Just

-- Internal Utils

map2 :: (a -> b -> c) -> Parser a -> Parser b -> Parser c
map2 f (P left) (P right) = P \input -> case left input of
  Bad                  -> Bad
  Good input valueLeft -> case right input of
    Bad                   -> Bad
    Good input valueRight -> Good input (f valueLeft valueRight)

andThen :: Parser a -> (a -> Parser b) -> Parser b
andThen (P parse) f = P \input ->
  case parse input of
    Bad              -> Bad
    Good input value -> let (P parse) = f value
                        in  parse input
