module Command
  ( Cmd
  , mv
  , Command (..)
  , runCmd
  , runCmd_
  , runCommand
  ) where

import Control.Exception hiding (throw)
import Data.Functor (void)
import Error
import Polysemy
import Polysemy.Error
import System.Process (readProcess)

type Cmd = (String, [String])

mv :: FilePath -> FilePath -> Cmd
mv src dest = ("mv", [src, dest])

ssh :: String -> Int -> Cmd -> Cmd
ssh host port (cmd, args) = ("ssh", [host, "-p", show port, cmd] ++ args)

data Command m a where
  RunCmd  :: Cmd -> Command m String
  RunCmd_ :: Cmd -> Command m ()
makeSem ''Command

runCommand :: Members [Embed IO, Error ErrorKind] effs => Bool -> Sem (Command ': effs) a -> Sem effs a
runCommand dryRun = interpret \case
  RunCmd  cmd -> (if dryRun then logCmd  else execCmd ) cmd
  RunCmd_ cmd -> (if dryRun then logCmd_ else execCmd_) cmd
  where
    execCmd, logCmd  :: Members '[Embed IO, Error ErrorKind] effs => Cmd -> Sem effs String
    execCmd_,logCmd_ :: Members '[Embed IO, Error ErrorKind] effs => Cmd -> Sem effs ()
    execCmd  (cmd, args) = (embed $ try $ readProcess cmd args "") `onErr` (throw . CommandError)
    execCmd_ cmd         = void  $ execCmd_ cmd
    logCmd_  (cmd, args) = embed $ void $ putStrLn $ unwords (cmd : args)
    logCmd   cmd         = logCmd_ cmd >> execCmd cmd

