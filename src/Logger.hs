module Logger where

import Polysemy
import Polysemy.Trace

data Logger m a where
  Log :: String -> Logger m ()
makeSem ''Logger

runLogger :: Member (Embed IO) effs => Sem (Logger ': effs) a -> Sem effs a
runLogger = interpret \case
  Log l -> embed $ putStrLn l

runLoggerPure :: Sem (Logger ': effs) a -> Sem effs ([String], a)
runLoggerPure = runTraceList . reinterpret \case
  Log l -> trace l