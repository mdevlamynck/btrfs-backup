module Error
  ( ErrorKind (..)
  , Error.runError
  , onErr
  ) where

import Control.Monad ((>=>))
import Polysemy
import Polysemy.Error as P
import System.IO

data ErrorKind
  = LoadConfig IOError
  | CommandError IOError
  | VolumeInfo

instance Show ErrorKind where
  show (LoadConfig e)   = "Failed to read configuration: " <> show e
  show (CommandError e) = "Command error: " <> show e
  show VolumeInfo       = "Failed to parse list of subvolumes info."

runError :: Member (Embed IO) effs => Sem (Error ErrorKind ': effs) () -> Sem effs ()
runError = P.runError >=> \case
  Left e  -> embed $ printErr e
  Right v -> pure v
  where
    printErr :: Show a => a -> IO ()
    printErr = hPrint stderr

-- | In a monadic context, extracts a value from a type potentially containing
-- | an error using a short-circuiting monadic result on error.
class OnErr f where
  type ErrorContainer f
  onErr :: Monad m => m (f a) -> (ErrorContainer f -> m a) -> m a

instance OnErr Maybe where
  type ErrorContainer Maybe = ()
  onErr m default_ = m >>= \case
    Nothing -> default_ ()
    Just a  -> return a

instance OnErr (Either e) where
  type ErrorContainer (Either e) = e
  onErr m default_ = m >>= \case
    Left e  -> default_ e
    Right a -> return a
