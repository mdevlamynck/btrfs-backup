module Config
  ( Config, VolumeConfig, SubvolumeConfig (..), TargetConfig (..)
  , Conf (..)
  , readConf
  , runConf
  , runConfStub
  , toConfig
  ) where

import           Data.List hiding (groupBy)
import           Data.List.Split (split, keepDelimsL, whenElt)
import           Data.Function ((&))
import           Data.Functor ((<&>))
import           Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import           Data.Maybe
import           Error
import           FileSystem as FS
import           Logger
import           Parser
import           Polysemy
import           Polysemy.Error
import           Polysemy.State
import           Prelude hiding (sequence, log)
import           Scheduler
import           Utils

type Config = Map FilePath VolumeConfig

type VolumeConfig = Map FilePath SubvolumeConfig

data SubvolumeConfig = SubvolumeConfig
  { snapshotDir      :: FilePath
  , snapshotSchedule :: Schedule
  , targets          :: [TargetConfig]
  }
  deriving (Eq, Show)

data TargetConfig = TargetConfig
  { targetPath     :: FilePath
  , targetSchedule :: Schedule
  }
  deriving (Eq, Show)

data Conf m a where
  ReadConf :: Conf m Config
makeSem ''Conf

runConf :: Members '[FileSystem, Error ErrorKind, Logger] effs => FilePath -> Sem (Conf ': effs) a -> Sem effs a
runConf configPath = evalState (Nothing :: Maybe Config) . reinterpret \case
  ReadConf -> do
    maybeConfig <- get
    case maybeConfig of
      Just config -> pure config
      Nothing     -> do
        file   <- FS.readFile configPath `onErr` (throw . LoadConfig)
        config <- toConfig file
        put $ Just config
        pure config

runConfStub :: Config -> Sem (Conf ': effs) a -> Sem effs a
runConfStub config = interpret \case
  ReadConf -> pure config

type WithLog effs a = Member Logger effs => Sem effs a

toConfig :: String -> WithLog effs Config
toConfig =
  parseConfig
    ( sections "volume" $
        subSections "subvolume" buildSubvolume
    )
    ( [ ("volume",            stringP   <&> Section)
      , ("subvolume",         stringP   <&> Section)
      , ("snapshot_dir",      stringP   <&> SubvolumeField . \s c -> c { snapshotDir = s })
      , ("snapshot_preserve", scheduleP <&> SubvolumeField . \s c -> c { snapshotSchedule = s })
      ]
    )

data ConfigField
  = Section String
  | SubvolumeField (SubvolumeConfig -> SubvolumeConfig)

type ConfigLine = (String, ConfigField)

stringP :: Parser String
stringP = many (charIf $ const True)

scheduleP :: Parser Schedule
scheduleP =
  succeed (foldl' (&) (Schedule None None None None None))
    |= sequence whitespaces
      ( succeed (&)
        |= oneOf
          [ Nb  <$> int
          , All <$  symbol "*"
          ]
        |= oneOf
          [ (\nb s -> s { perYear  = nb }) <$ symbol "y"
          , (\nb s -> s { perMonth = nb }) <$ symbol "m"
          , (\nb s -> s { perWeek  = nb }) <$ symbol "w"
          , (\nb s -> s { perDay   = nb }) <$ symbol "d"
          , (\nb s -> s { perHour  = nb }) <$ symbol "h"
          ]
      )

parseConfig :: ([(String, ConfigField)] -> Config) -> [(String, Parser ConfigField)] -> String -> WithLog effs Config
parseConfig builder parsers input = do
  maybeConfigLines <- (logErrors . parseLine parsers) `mapM` splitLines input
  pure $ builder (catMaybes maybeConfigLines)
  where
    splitLines =
      mapMaybe (tupleHeadMay . words . takeWhile (/= '#') . trim) . lines

    tupleHeadMay = \case
      (head:tail) -> Just (head, unwords tail)
      _           -> Nothing

    parseLine parsers (key, values) =
      case Map.lookup key $ Map.fromList parsers of
        Nothing     -> Left $ "Warning: Unknown config key \"" <> key <> "\", ignoring."
        Just parser -> case runParser parser values of
          Nothing    -> Left $ "Warning: Failed to parse value for key \"" <> key <> "\", ignoring."
          Just value -> Right (key, value)

    logErrors = \case
      Left e  -> log e >> pure Nothing
      Right v -> pure $ Just v

sections :: String -> ([(String, ConfigField)] -> [(String, ConfigField)] -> a) -> [(String, ConfigField)] -> Map String a
sections key builder = subSections key builder []

subSections :: String -> ([(String, ConfigField)] -> [(String, ConfigField)] -> a) -> [(String, ConfigField)] -> [(String, ConfigField)] -> Map String a
subSections key builder common lines =
  Map.fromList $ section (common ++ (concat subCommon)) <$> sectionsFields
  where
    (sectionsFields, subCommon) = groupBy key lines
    groupBy key = partition (isPrefixOf [key] . fmap fst) . split (keepDelimsL $ whenElt (\(k, _) -> k == key))
    section common = \case
      (_, Section v) : rest -> (v, builder common rest)
      (key, _) : _          -> error $ "\"" <>key <> "\" should be a section delimiter, config definition is incoherent, should not happen."
      _                     -> error $ "Empty section, should not happen."

buildSubvolume :: [ConfigLine] -> [ConfigLine] -> SubvolumeConfig
buildSubvolume common fields =
  foldl apply empty (common ++ fields)
  where
    empty = SubvolumeConfig "" (Schedule None None None None None) []
    apply acc = \case
      (_, SubvolumeField f) -> f acc
      _                     -> acc
