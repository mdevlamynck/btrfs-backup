module Btrfs
  ( Btrfs (..)
  , readVolumeInfo
  , createSnapshot
  , deleteSnapshot
  , restoreSnapshot
  , sendBackup
  , deleteBackup
  , runBtrfs
  ) where

import BtrfsCli
import Command
import Error
import Parser (runParser)
import Polysemy
import Polysemy.Error

data Btrfs m a where
  ReadVolumeInfo  :: FilePath -> Btrfs m [Subvolume]
  CreateSnapshot  :: (FilePath, FilePath) -> Btrfs m ()
  DeleteSnapshot  :: FilePath -> Btrfs m ()
  RestoreSnapshot :: (FilePath, FilePath) -> Btrfs m ()
  SendBackup      :: (FilePath, FilePath) -> Btrfs m ()
  DeleteBackup    :: FilePath -> Btrfs m ()
makeSem ''Btrfs

runBtrfs :: Members '[Command, Error ErrorKind] effs => Sem (Btrfs ': effs) a -> Sem effs a
runBtrfs = interpret \case
  ReadVolumeInfo volume            ->
    (runParser parserSubvolumeList <$> runCmd (subvolumeList volume))
      `onErr` (const $ throw VolumeInfo)
  CreateSnapshot (source, target)  -> runCmd_ $ subvolumeSnapshot source target
  DeleteSnapshot target            -> runCmd_ $ subvolumeDelete target
  RestoreSnapshot (source, target) -> do
    runCmd_ $ mv target (target <> ".old")
    runCmd_ $ subvolumeFromSnapshot source target
    runCmd_ $ subvolumeDelete (target <> ".old")
  SendBackup (source, target)      -> pure ()
  DeleteBackup target              -> pure ()
