module BtrfsCli
  ( Subvolume(..), UUID
  , subvolumeList, parserSubvolumeList
  , subvolumeSnapshot
  , subvolumeFromSnapshot
  , subvolumeDelete
  ) where

import Parser
import Command

type UUID = String

-- Subvolume List

data Subvolume = Subvolume
  { subvolPath   :: FilePath
  , subvolUuid   :: UUID
  , parentUuid   :: UUID
  , receivedUuid :: Maybe UUID
  }
  deriving (Eq, Show)

subvolumeList :: FilePath -> Cmd
subvolumeList volume =
  ("btrfs"
  , [ "subvolume"
    , "list"
    , volume
    , "-o" -- ^ only subvolumes bellow path
    , "-a" -- ^ distinguish between absolute and relative subvolumes path relative to volume path
    , "-u" -- ^ subvolume uuid
    , "-q" -- ^ parent uuid
    , "-R" -- ^ received uuid
    , "-s" -- ^ Snapshots only
    , "-r" -- ^ ReadOnly Subvolumes only
    ]
  )

parserSubvolumeList :: Parser [Subvolume]
parserSubvolumeList =
  many $
    succeed (\parent received subvol path -> Subvolume path subvol parent received)
      |. keyValue (keyword "ID")            word
      |. keyValue (keyword "gen")           word
      |. keyValue (keyword "cgen")          word
      |. keyValue (keyword "top level")     word
      |. keyValue (keyword "otime")         (word |. whitespaces |. word)
      |= keyValue (keyword "parent_uuid")   word
      |= keyValue (keyword "received_uuid")
        ( oneOf
            [ Just    <$> word
            , Nothing <$  symbol "-"
            ]
        )
      |= keyValue (keyword "uuid")          word
      |= keyValue (keyword "path")          word
      |. lineReturn

-- Subvolume Snapshot

subvolumeSnapshot :: FilePath -> FilePath -> Cmd
subvolumeSnapshot source target = ("btrfs", [ "subvolume", "snapshot", source, target, "-r" ])

subvolumeFromSnapshot :: FilePath -> FilePath -> Cmd
subvolumeFromSnapshot source target = ("btrfs", [ "subvolume", "snapshot", source, target ])

subvolumeDelete :: FilePath -> Cmd
subvolumeDelete target = ("btrfs", [ "subvolume", "delete", target ])

-- Utils

keyValue :: Parser a -> Parser value -> Parser value
keyValue key value =
  succeed id
    |. key
    |. whitespaces
    |= value
    |. optional whitespaces

word :: Parser String
word =
  while (`elem` ['a' .. 'z'] <> ['A' .. 'Z'] <> ['0' .. '9'] <> ['-', ':', '/', '.'])
    >>= \case
      "-"   -> failed
      value -> succeed value
