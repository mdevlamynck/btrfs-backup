module Time
  ( Time (..)
  , UTCTime
  , readCurrentTime
  , runTime
  , runTimeStub
  , Time.formatTime
  ) where

import Data.Time
import Data.Time.Format as FormatTime (defaultTimeLocale, formatTime)
import Polysemy

data Time m a where
  ReadCurrentTime :: Time m UTCTime
makeSem ''Time

runTime :: Member (Embed IO) effs => Sem (Time ': effs) a -> Sem effs a
runTime = interpret \case
  ReadCurrentTime -> embed getCurrentTime

runTimeStub :: UTCTime -> Sem (Time ': effs) a -> Sem effs a
runTimeStub time = interpret \case
  ReadCurrentTime -> pure time

formatTime :: UTCTime -> String
formatTime = FormatTime.formatTime defaultTimeLocale "%0Y-%m-%dT%H:%M"
